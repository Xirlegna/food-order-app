var appController = (function() {
    var Item = function(id, name, price, img) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
    };

    var data = {
        Items: {
            food: [],
            drink: [],
            dessert: []
        }
    };

    return {
        initItems: function() {
            data.Items['food'].push(new Item(0, 'Pizza', 1400, 'pizza.jpg'));
            data.Items['food'].push(new Item(1, 'Lasagne', 1100, 'lasagne.jpg'));
            data.Items['food'].push(new Item(2, 'Hamburger', 1600, 'hamburger.jpg'));
            data.Items['food'].push(new Item(3, 'Quesadilla', 1550, 'quesadilla.jpg'));

            data.Items['drink'].push(new Item(4, 'Alma lé', 700, 'apple-juice.jpg'));
            data.Items['drink'].push(new Item(5, 'Kék cocktail', 1200, 'blue-cocktail.jpg'));
            data.Items['drink'].push(new Item(6, 'Színes cocktail', 1200, 'rainbow-cocktail.jpg'));
            data.Items['drink'].push(new Item(7, 'Kévé', 1200, 'coffee.jpg'));

            data.Items['dessert'].push(new Item(8, 'Almás pite', 1800, 'apple-pie.jpg'));
            data.Items['dessert'].push(new Item(9, 'Jégkrém', 600, 'ice-cream.jpg'));
            data.Items['dessert'].push(new Item(10, 'Torta', 600, 'cake.jpg'));
            data.Items['dessert'].push(new Item(10, 'Macaron', 1350, 'macaron.jpg'));
        },

        getItems: function(type) {
            return data.Items[type];
        }
    };
})();

var UIController = (function() {

    var DOMstrings = {
        routerFood: '.sidebar__button--food',
        routerDrink: '.sidebar__button--drink',
        routerDessert: '.sidebar__button--dessert',
        foodList: '.food-list'
    };

    return {
        drawItems: function(items) {
            document.querySelector(DOMstrings.foodList).innerHTML = '';

            for (var i = 0; i < items.length; i++) {
                var obj, html;

                obj = items[i];

                html = '<div class="food"><img src="img/%img%" alt="Quesadilla" class="food__img"><p class="food__price">%price% Ft</p><h5 class="food__name">%name%</h5><div class="food__add-btn"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="food__icon"><path d="M12 29c0 1.657-1.343 3-3 3s-3-1.343-3-3c0-1.657 1.343-3 3-3s3 1.343 3 3z"></path><path d="M32 29c0 1.657-1.343 3-3 3s-3-1.343-3-3c0-1.657 1.343-3 3-3s3 1.343 3 3z"></path><path d="M32 16v-12h-24c0-1.105-0.895-2-2-2h-6v2h4l1.502 12.877c-0.915 0.733-1.502 1.859-1.502 3.123 0 2.209 1.791 4 4 4h24v-2h-24c-1.105 0-2-0.895-2-2 0-0.007 0-0.014 0-0.020l26-3.98z"></path></svg></div></div>';

                newHtml = html.replace('%name%', obj.name);
                newHtml = newHtml.replace('%price%', obj.price);
                newHtml = newHtml.replace('%img%', obj.img);

                document.querySelector(DOMstrings.foodList).insertAdjacentHTML('beforeend', newHtml);
            }
        },

        getDOMstrings: function() {
            return DOMstrings;
        }
    };

})();

var controller = (function(appCtrl, UICtrl) {

    var setupEventListeners = function() {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.routerFood).addEventListener('click', function() {
            UICtrl.drawItems(appCtrl.getItems('food'));
        });

        document.querySelector(DOM.routerDrink).addEventListener('click', function() {
            UICtrl.drawItems(appCtrl.getItems('drink'));
        });

        document.querySelector(DOM.routerDessert).addEventListener('click', function() {
            UICtrl.drawItems(appCtrl.getItems('dessert'));
        });
    };

    return {
        init: function() {
            console.log('Application has started.');
            appCtrl.initItems();

            UICtrl.drawItems(appCtrl.getItems('food'));

            setupEventListeners();
        }
    };

})(appController, UIController);

controller.init();